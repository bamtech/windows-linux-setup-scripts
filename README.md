# Windows Setup

Just an easy way to install some software because I tinker too much and reinstall distributions of everything. 

## ToDo

### Linux Stuff
- [ ] add some base setup dnf scripts (:heart: fedora)
- [ ] add some base setup apt scripts
- [ ] add some desktop setup scripts for cinnamon? or gnome?
- [ ] change the title to Windows and Linux setup when needed

### Windows Stuff
- [ ] add the docker-compose yml files for home server setup
