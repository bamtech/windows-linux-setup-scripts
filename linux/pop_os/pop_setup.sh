sudo apt remove --purge -y libreoffice*

sudo apt install -y zsh neofetch flatpak

flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Install browsers
flatpak install -y flathub io.gitlab.librewolf-community \
    com.github.micahflee.torbrowser-launcher \
    flathub com.brave.Browser

# Install Office and Productivity
flatpak install -y flathub org.onlyoffice.desktopeditors \
    org.gnome.Evolution \
    md.obsidian.Obsidian \
    github.phase1geo.minder

# Install Dev Tools
flatpak install -y flathub com.vscodium.codium \
    com.getpostman.Postman

# Install Entertainment and other ish
flatpak install -y flathub com.spotify.Client \
    org.qbittorrent.qBittorrent \
    com.mattjakeman.ExtensionManager \
    com.leinardi.gwe

# Install Java and Maven
sudo apt install -y openjdk-18-jdk-headless maven
