# Remove unwanted stuff
sudo dnf remove -y gnome-maps gnome-tour gnome-connections rhythmbox cheese libreoffice*

# Add Repos - flatpak and RPM Fusion Free
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update -y

# Nvidia Drivers
sudo dnf install -y akmod-nvidia xorg-x11-drv-nvidia-cuda

# Add multimedia groups
sudo dnf groupupdate -y core
sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
sudo dnf groupupdate -y sound-and-video

# System Tools
sudo dnf install -y fish neofetch

# Install browsers
flatpak install -y flathub io.gitlab.librewolf-community \
    com.github.micahflee.torbrowser-launcher \
    flathub com.brave.Browser

# Install Communications
flatpak install -y flathub com.discordapp.Discord \
    org.signal.Signal \
    com.slack.Slack

# Install Office and Productivity
flatpak install -y flathub org.onlyoffice.desktopeditors \
    org.gnome.Evolution \
    md.obsidian.Obsidian \
    github.phase1geo.minder

# Install Dev Tools
flatpak install -y flathub com.vscodium.codium \
    com.getpostman.Postman

# Install Entertainment and other ish
flatpak install -y flathub com.spotify.Client \
    org.qbittorrent.qBittorrent \
    com.mattjakeman.ExtensionManager \
    com.leinardi.gwe

# Install Java and Maven
sudo dnf install -y java-17-openjdk-devel maven
